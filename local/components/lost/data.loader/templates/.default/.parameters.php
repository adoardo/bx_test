<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var string $componentPath
 * @var string $componentName
 * @var array $arTemplateParameters
 */

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Web\Json;
use Bitrix\Iblock;

if (!Loader::includeModule('iblock'))
	return;

$boolCatalog = Loader::includeModule('catalog');
CBitrixComponent::includeComponentClass($componentName);

$usePropertyFeatures = Iblock\Model\PropertyFeature::isEnabledFeatures();

$iblockExists = (!empty($arCurrentValues['IBLOCK_ID']) && (int)$arCurrentValues['IBLOCK_ID'] > 0);

$defaultValue = array('-' => GetMessage('CP_BCS_TPL_PROP_EMPTY'));
$arSKU = false;
$boolSKU = false;
$filterDataValues = array();
if ($boolCatalog && (isset($arCurrentValues['IBLOCK_ID']) && 0 < intval($arCurrentValues['IBLOCK_ID'])))
{
	$arSKU = CCatalogSku::GetInfoByProductIBlock($arCurrentValues['IBLOCK_ID']);
	$boolSKU = !empty($arSKU) && is_array($arSKU);
	$filterDataValues['iblockId'] = (int)$arCurrentValues['IBLOCK_ID'];
	if ($boolSKU)
	{
		$filterDataValues['offersIblockId'] = $arSKU['IBLOCK_ID'];
	}
}

$arAllPropList = array();
$arFilePropList = $defaultValue;
$arListPropList = array();

$highloadblocksarray = array();

// if ($iblockExists)
// {
// 	$rsProps = CIBlockProperty::GetList(
// 		array('SORT' => 'ASC', 'ID' => 'ASC'),
// 		array('IBLOCK_ID' => $arCurrentValues['IBLOCK_ID'], 'ACTIVE' => 'Y')
// 	);
// 	while ($arProp = $rsProps->Fetch())
// 	{
// 		$strPropName = '['.$arProp['ID'].']'.('' != $arProp['CODE'] ? '['.$arProp['CODE'].']' : '').' '.$arProp['NAME'];

// 		if ($arProp['CODE'] == '')
// 		{
// 			$arProp['CODE'] = $arProp['ID'];
// 		}

// 		$arAllPropList[$arProp['CODE']] = $strPropName;

// 		if ($arProp['PROPERTY_TYPE'] === 'F')
// 		{
// 			$arFilePropList[$arProp['CODE']] = $strPropName;
// 		}

// 		if ($arProp['PROPERTY_TYPE'] === 'L')
// 		{
// 			$arListPropList[$arProp['CODE']] = $strPropName;
// 		}
// 	}

// 	$showedProperties = [];
// 	if ($usePropertyFeatures)
// 	{
// 		if ($iblockExists)
// 		{
// 			$showedProperties = Iblock\Model\PropertyFeature::getListPageShowPropertyCodes(
// 				$arCurrentValues['IBLOCK_ID'],
// 				['CODE' => 'Y']
// 			);
// 			if ($showedProperties === null)
// 				$showedProperties = [];
// 		}
// 	}
// 	else
// 	{
// 		if (!empty($arCurrentValues['PROPERTY_CODE']) && is_array($arCurrentValues['PROPERTY_CODE']))
// 		{
// 			$showedProperties = $arCurrentValues['PROPERTY_CODE'];
// 		}
// 	}
// 	if (!empty($showedProperties))
// 	{
// 		$selected = array();

// 		foreach ($showedProperties as $code)
// 		{
// 			if (isset($arAllPropList[$code]))
// 			{
// 				$selected[$code] = $arAllPropList[$code];
// 			}
// 		}

// 		$arTemplateParameters['PROPERTY_CODE_MOBILE'] = array(
// 			'PARENT' => 'VISUAL',
// 			'NAME' => GetMessage('CP_BCS_TPL_PROPERTY_CODE_MOBILE'),
// 			'TYPE' => 'LIST',
// 			'MULTIPLE' => 'Y',
// 			'VALUES' => $selected
// 		);
// 	}
// 	unset($showedProperties);
// }	