<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Application,
    \Bitrix\Iblock\Component\Tools;


class ADVComponent extends CBitrixComponent
{

    private $componentPage = 'index';

    private function getPage()
    {
        $request = Application::getInstance()->getContext()->getRequest();

        $uriString = array_filter(explode("/", $request->getRequestedPage()), function ($var) {
            if ($var != 'index.php') {
                return $var;
            } else {
                return null;
            }
        }

        );

        if (count($uriString) > 1) {

            Tools::process404(
                ""
                , "Y"
                , "Y"
                , "Y"
                , $this->arParams["FILE_404"]
            );

        }

    }

    public function executeComponent()
    {
        $this->getPage();
        $this->includeComponentTemplate($this->componentPage);

    }

}