<?php
if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

Loader::includeModule("highloadblock");
Loader::includeModule("iblock");

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$highloadblocks = \Bitrix\Highloadblock\HighloadBlockTable::getList();

$highloadblocksarray = array();

while($value = $highloadblocks->fetch()) {
    $highloadblocksarray[$value['ID']] = $value['NAME'];
}

$arIBlock = array();
$iblockFilter = !empty($arCurrentValues['IBLOCK_TYPE'])
    ? array('TYPE' => $arCurrentValues['IBLOCK_TYPE'], 'ACTIVE' => 'Y')
    : array('ACTIVE' => 'Y');

$rsIBlock = CIBlock::GetList(array('SORT' => 'ASC'), $iblockFilter);
while ($arr = $rsIBlock->Fetch())
{
    $id = (int)$arr['ID'];
    if (isset($offersIblock[$id]))
        continue;
    $arIBlock[$id] = '['.$id.'] '.$arr['NAME'];
}

$arComponentParameters = [
    "GROUPS" => [],
    "PARAMETERS" => [
        'HL_BLOCK_ID' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('CP_BCS_TPL_HIGHLOAD_PRICES_ADV'),
            'TYPE' => 'LIST',
            'VALUES' => $highloadblocksarray
        ),
        "AJAX_MODE" => [],
        "SEF_MODE" => [
            "detail" => [
                "NAME" => Loc::getMessage("DETAIL_PAGE"),
                "DEFAULT" => "#ELEMENT_ID#/",
                "VARIABLES" => [
                    "ELEMENT_ID",
                    "ELEMENT_CODE",
                    "SECTION_ID",
                    "SECTION_CODE",
                ],
            ],
            "edit" => [
                "NAME" => Loc::getMessage("EDIT_PAGE"),
                "DEFAULT" => "#ELEMENT_ID#/edit/",
                "VARIABLES" => [
                    "ELEMENT_ID",
                    "ELEMENT_CODE",
                    "SECTION_ID",
                    "SECTION_CODE",
                ],
            ],
        ],
        "CACHE_TIME"  =>  ["DEFAULT"=>86400],
    ],
];