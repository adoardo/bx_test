<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>

<div id="content">
    <?php if(!empty($arResult['ITEMS'])){ ?>
		<div id="list">
			<table>
				<tr id="header-list">
					<td class="tqCenter">Файл выгрузки</td>
					<td>Дата</td>
					<td>Пользователь</td>
				</tr>
                <?php foreach ($arResult['ITEMS'] as $ip => $item){
                    $file = [];
                	if(!empty($item['UF_FILE'])){
                		$file = CFile::GetFileArray($item['UF_FILE']);
	                }
                	if(empty($file)) continue;
                ?>
					<tr>
						<td><a download href="<?= $file['SRC'] ?>"><?=$file['ORIGINAL_NAME']?></a></td>
						<td>
							<?=$item['UF_DATE']?>
						</td>
						<td>
							<?=$item['UF_USER_NAME']?>
						</td>
					</tr>
                <?php } ?>
			</table>
			<?=$arResult['PAGER']?>
		</div>
    <?php } else { ?>
		<h2 class="empty-header">Данные отсутвуют</h2>
    <?php } ?>
</div>
<?if($_REQUEST['AJAX_CALL'] == 'Y'){?>
<script>
        BX.closeWait();
</script>
<?}?>