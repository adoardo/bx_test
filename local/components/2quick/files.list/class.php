<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Application,
    Bitrix\Main\Loader,
    \Bitrix\Iblock\Component\Tools;


Loader::includeModule('iblock');
Loader::includeModule("highloadblock");
class AdvListComponent extends CBitrixComponent
{
    public static $pagintaionStr;

    /**
     * @param $arElements
     * @return object
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    private function getItems(){
        $result = [];

            $block = HL\HighloadBlockTable::getById($this->arParams['HL_BLOCK_ID']);
            if ($hlblock = $block->fetch()) {

                $entity = HL\HighloadBlockTable::compileEntity($hlblock);
                $entity_data_class = $entity->getDataClass();

                $res = $entity_data_class::getList(array(
                        'filter' => array(),
                        'select' => array("*"),
                        'order' => array(
                            'ID' => 'desc'
                        ),
                    )
                );
                $result = self::makePagination($res);
            }

        return $result;
    }

    /**
     * 
     */
    private function getResult()
    {

            $this->arResult['ITEMS'] = $this->getItems()->arResult;
            $this->arResult['PAGER'] = self::$pagintaionStr;
        
            $request = Application::getInstance()->getContext()->getRequest();
            $uriString = array_filter(explode("/", $request->getRequestedPage()), function ($var) {
                if ($var != 'index.php') {
                    return $var;
                } else {
                    return null;
                }
            }
            );

        if (count($uriString) > 1) {
            Tools::process404(
                ""
                , "Y"
                , "Y"
                , "Y"
                , $this->arParams["FILE_404"]
            );
        }

    }

    public static function makePagination($res)
    {
        $rows = array();
        while ($row = $res->fetch()) {
            $rows[] = $row;
        }
        $res = new \CDBResult;
        $res->InitFromArray($rows);

        $intItemPerPage = (!empty($_GET['SIZEN_2'])?$_GET['SIZEN_2']:10);
        $intCurPage = (!empty($_GET['PAGEN_2'])?$_GET['PAGEN_2']:1);


        $res->navStart($intItemPerPage,false,$intCurPage );

        self::$pagintaionStr = $res->GetPageNavStringEx($navComponentObject, '', 'round',false);

        return $res;
    }

    public static function getPagination()
    {
        return self::$pagintaionStr;
    }

    /**
     * @return mixed|void
     */
    public function executeComponent()
    {

        $this->getResult();
        $this->includeComponentTemplate();

    }

}