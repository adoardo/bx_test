<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="bx_catalog_text" id="main-menu">
    <? //pre($arResult);
    if (!empty($arResult)) {
        $loaderImg = getLoaderRotation(); ?>
        <ul class="bx_catalog_text_ul">
            <? foreach ($arResult['MENU'] as $arItem) {
                if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                    continue;

                $liClass = 'bx_catalog_text_title';
                $amount = '';
                if ($arItem['AMOUNT']) {
                    if ($arResult['GET_AMOUNT'] && !$arResult['AJAX']) {
                        $liClass .= ' amount';

                        $ellipse = is_int($arItem['AMOUNT']) ? '' : 'ellipse';
                        $amount = '<div class="' . $ellipse . '">' . $arItem['AMOUNT'] . '</div>';

                    } else {
                        $amount = '<div class="get-amount">' . $loaderImg . '</div>';
                    }
                }
                if ($arItem['SELECTED']) $liClass .= ' selected';
                ?>
                <li class="<?= $liClass; ?>" <?= $arItem["LINK_CODE"] ? 'data-amount="' . $arItem["LINK_CODE"] . '"' : '' ?>>
                    <a href=" <?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                    <?= $amount; ?>
                </li>
            <? } ?>
        </ul>
    <? } ?>
</div>