<?
$arResult = array(
    'MENU' => $arResult,
    'GET_AMOUNT' => isset($_REQUEST['get_amount']) && $_REQUEST['get_amount'] == 'main_menu',
    'AJAX' => isset($_REQUEST['ajax']) && $_REQUEST['ajax'] == 'Y'
);

if ($arResult['AJAX'])
    if (!CModule::IncludeModule('iblock'))
        exit(0);

foreach ($arResult['MENU'] as &$arItem) {
    $link = explode('?', strtolower($arItem['LINK']))[0];//remove $_GET params
    $partitions = array_values(array_filter(explode('/', $link), function ($partition) {//remove *.(php/html/ etc) file & empty strings
        if (!mb_strlen($partition)) return false;
        $isPhp = mb_strpos($partition, '.php');
        if ($isPhp !== false) return false;
        $isHtml = mb_strpos($partition, '.html');
        if ($isHtml !== false) return false;
        return true;
    }));

    if ($arItem['AMOUNT'])
        $arItem['LINK_CODE'] = $arResult['GET_AMOUNT'] && !$arResult['AJAX'] ? 'amount' : $partitions[0];
}
unset($arItem);

if ($arResult['GET_AMOUNT'] && $arResult['AJAX']) {
    $out = array();
    foreach ($arResult['MENU'] as $arItem) {
        if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
            continue;
        if (!$arItem['AMOUNT']) continue;

        $ellipse = $arItem['AMOUNT'] && is_int($arItem['AMOUNT']) ? '' : 'ellipse';
        $amount = $arItem['AMOUNT'] ? '<div class="' . $ellipse . '">' . $arItem['AMOUNT'] . '</div>' : '';

        $out[$arItem['LINK_CODE']] = $amount;
    }
    exitJson($out, true);
}