<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!DOCTYPE html>
<html>
<head>
    <meta name="robots" content="noindex,nofollow"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8"/>
    <title><? $APPLICATION->ShowTitle() ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="<?= SITE_TEMPLATE_PATH; ?>/images/favicon.ico"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha256-Kg2zTcFO9LXOc7IwcBx1YeUBJmekycsnTsq2RuFHSZU=" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.js" integrity="sha256-gQzieXjKD85Ibbpg4l8GduIagpt4oUSQRYaDaLd+8sI=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/jquery/script.js?v=1.0"></script>
    <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/jquery/jquery.timer.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" integrity="sha256-rByPlHULObEjJ6XQxW/flG2r+22R5dKiAoef+aXWfik=" crossorigin="anonymous" />
    <link rel="stylesheet" media="all" type="text/css" href="<?= SITE_TEMPLATE_PATH ?>/fancybox/jquery.fancybox.css?v=2.1.5"/>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.css" integrity="sha256-AbZqn2w4KXugIvUu6QtV4nK4KlXj4nrIp6x/8S4Xg2U=" crossorigin="anonymous" />
    <? $APPLICATION->ShowHead() ?>
</head>
<body>
<? $APPLICATION->ShowPanel(); ?>
<div class="year-time-block">
    <div id="main-bg">
        <div id="header">
            <?/*<a href="/"><img src="/images/logo1.png" width="100px"/><img src="/images/logo2.png" width="100px"/><img
                    src="/images/logo3.png" width="150px"/></a><br/>*/?>
            <br/>
            <?
            CModule::IncludeModule('iblock');
            if ($USER->IsAuthorized()) { ?>
                <div class="col-15">
                    <? $APPLICATION->IncludeComponent("bitrix:system.auth.form", "", Array()); ?>
                </div>
                <div class="col-50">&nbsp;</div>
                <?
                //set privilegies by group
                $GLOBALS["USER_GROUPS"] = $USER->GetUserGroupArray();
                //admin + administrator
                if (checkUsersGroupPermission(array(1,9))) $GLOBALS["USIR_GROUPZ"] = 'administrator';
                elseif (checkUsersGroupPermission(array(7, 25))) $GLOBALS["USIR_GROUPZ"] = 'manager';
                elseif (checkUsersGroupPermission(8)) $GLOBALS["USIR_GROUPZ"] = 'courier';
            }?>
        </div>

        <div id="main-container">
            <?if(md5($_SERVER['HTTP_HOST'])!='da4da08eed29fd49903e55803dda14fb'){?><style>#testserver{text-align: center;border-top: 7px double deepskyblue;margin: 0 -5px;padding: 15px;font-weight: bold;font-family: monospace;font-size: 25px;border-bottom: 7px double deeppink;color: white;background: black}</style><div id="testserver">! ЭТО <?=$_SERVER['HTTP_HOST'];?> СЕРВЕР !</div><?}?>
            <? if ($USER->IsAuthorized()) { ?>
                <? $APPLICATION->IncludeComponent("bitrix:menu", ".default", Array(
                        "ROOT_MENU_TYPE" => "top",
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "top",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "86400",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => ""
                    )
                ); ?>
                <?

                if(isset($_GET['whorderscome']) || $USER->GetID() == 25)
                    require __DIR__.'/components/_tsk_whkeeper/_orders_come.php';
                ?>
            <? } ?>