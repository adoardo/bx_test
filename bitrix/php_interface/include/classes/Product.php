<?php

/*класс для работы со товарами*/

class Product
{
    const IBLOCK_ID = 17;

    const SECTION_SERVICE = 128;
    const SECTION_NEW = 178;
    const GPS_TRACKER_SECTION_ID = 211;
    const KVADRIK_MAIN_SECTION_ID = 143;
    const GIRIK_MAIN_SECTION_ID = 110;
    const SPY_CAMERA_SECTION_ID = 300;
    const NEODYM_MAGNETS = 95;
    const SEARCH_MAGNETS = 176;
    const SQUARE_MAGNETS = 177;
    const LOL_REPLICA = 315;
    const TRI_DE_PEN = 133;
    const MICRO_HEADPHONES = 93;
    const MEMORY_CARDS = 224;

    /*
     * получить товары всех раздела и его подразделов по ИД раздела
     */
    static function getSectionAllProducts($root)
    {
        $products = $sections = $arRoot = array();
        $root = is_array($root) ? array_map('intval', $root) : intval($root);
        $db = CIBlockSection::GetList(
            array(),
            array(
                'IBLOCK_ID' => self::IBLOCK_ID,
                'ACTIVE' => 'Y',
                'ID' => $root,
                'CHECK_PERMISSIONS' => 'N'
            ),
            false,
            array(
                'ID',
                'LEFT_MARGIN',
                'RIGHT_MARGIN',
                'DEPTH_LEVEL'
            )
        );
        while ($resRoot = $db->Fetch()) {
            if (!empty($arRoot)) {
                foreach ($arRoot as $key => $root) {
                    if ($resRoot['LEFT_MARGIN'] >= $root['LEFT_MARGIN']
                        && $resRoot['RIGHT_MARGIN'] <= $root['RIGHT_MARGIN']
                    ) continue 2;
                    elseif ($resRoot['LEFT_MARGIN'] <= $root['LEFT_MARGIN']
                        && $resRoot['RIGHT_MARGIN'] >= $root['RIGHT_MARGIN']
                    ) unset($arRoot[$key]);
                }
            }
            $arRoot[] = $resRoot;
        }

        if (empty($arRoot))
            return $products;

        //get subfolders
        foreach ($arRoot as $root) {
            $sections[] = $root['ID'];
            $db = CIBlockSection::GetList(
                array('LEFT_MARGIN' => 'ASC'),
                array(
                    '>DEPTH_LEVEL' => $root['DEPTH_LEVEL'],
                    'IBLOCK_ID' => self::IBLOCK_ID,
                    'ACTIVE' => 'Y',
                    '>LEFT_MARGIN' => $root['LEFT_MARGIN'],
                    '<RIGHT_MARGIN' => $root['RIGHT_MARGIN'],
                    'CHECK_PERMISSIONS' => 'N'
                ),
                false,
                array('ID')
            );
            while ($res = $db->Fetch())
                $sections[] = $res['ID'];
        }

        $sections = array_unique($sections);

        //get products of this foldets
        $db = CIBlockElement::GetList(
            array(),
            array(
                'IBLOCK_ID' => Product::IBLOCK_ID,
                'ACTIVE' => 'Y',
                'SECTION_ID' => $sections
            ),
            false,
            false,
            array('ID')
        );
        while ($res = $db->Fetch())
            $products[] = intval($res['ID']);

        return $products;
    }

    //получить все ID разделов с магнитами:
    static function getMagnetsSectionsID()
    {
        return array(
            self::NEODYM_MAGNETS,
            self::SEARCH_MAGNETS,
            self::SQUARE_MAGNETS
        );
    }

    //получить все ID разделов с гириками:
    static function getGirosSectionsID()
    {
        return array(
            self::GIRIK_MAIN_SECTION_ID,//гироскутер
//        116,//сигвеи
            117,//smart balance
            118,//transformer
            122,//transformer led
            119,//wheel suv
            120,//kids
            123,//genesis pro
            124,//scoolance
            125,//spaceboard
            132,//k-ox
            130,//optimus prime
            131,//rockwheel
            277,//smart one 10'5
        );
    }

    //получить все ID разделов с квадриками:
    static function getKvadriksSectionsID()
    {
        return array(
            191,//xiaomi
            190,//dji
            189,//hubsan
            188,//syma
        );
    }

    //получить все ID разделов с детскими часами:
    static function getKidWatchesSectionsID()
    {
        return array(
            195,//Детские часы G98 с GPS
            150,//Детские часы GW1000 (Q75)
            198,//Детские часы Gw600 (Q360, Q610) с GPS
            146,//Детские часы Q150
            147,//Детские часы Q50
            201,//Детские часы Q529 с GPS и камерой
            200,//Детские часы Q60 с GPS
            175,//Детские часы Q730 с GPS
            174,//Детские часы Q750 с GPS
            149,//Детские часы Q90
            151,//Детские часы T7 (G100)
            308,//Детские часы водонепроницаемые A32 с GPS
            309,//Детские часы водонепроницаемые DS-05 с GPS
            252,//Детские часы водонепроницаемые GW 300S (W8/Q520S) с GPS
            199,//Детские часы водонепроницаемые GW400S (W9, HW8) с GPS/
            192,//Детские часы водонепроницаемые K10 с GPS
            196,//Детские часы водонепроницаемые K3 с GPS
        );
    }

    //получить все ID разделов с квадриками:
    static function getLolDollsSectionsID()
    {
        return array(
            271,//root
            314,//original
            315,//replica
        );
    }

    //for kvadrik sections
    static function getNalPrice($price)
    {
        $price = intval($price);
        $priceNal = ceil($price / 1.1 / 10) * 10;
        return $priceNal > 10000 ? $priceNal : $price;
    }


    //получить АБСОЛЮТНО все ID разделов по квадрикам и аксессуарам:
    static function getFullSectionsID($mainSectionID)
    {
        $result = array();
        if (!$mainRes = CIBlockSection::GetList(
            array(),
            array(
                'IBLOCK_ID' => self::IBLOCK_ID,
                'ID' => $mainSectionID,//main section ID
                'CHECK_PERMISSIONS' => 'N'
            ),
            false,
            array('LEFT_MARGIN', 'RIGHT_MARGIN')
        )->Fetch()
        )
            return $result;

        $db = CIBlockSection::GetList(
            array(),
            array(
                'IBLOCK_ID' => self::IBLOCK_ID,
                '>=LEFT_MARGIN' => $mainRes['LEFT_MARGIN'],
                '<=RIGHT_MARGIN' => $mainRes['RIGHT_MARGIN'],
                'CHECK_PERMISSIONS' => 'N'
            ),
            false,
            array('ID')
        );
        while ($res = $db->Fetch())
            $result[] = $res['ID'];

        return $result;
    }


    //получить АБСОЛЮТНО все ID разделов по квадрикам и аксессуарам:
    static function getFullKvadriksSectionsID()
    {
        return self::getFullSectionsID(self::KVADRIK_MAIN_SECTION_ID);
    }

    /*
     * возврат ИД разделов, на которые действует уменьшение цены при переводе на карту либо расчет в офисе (тск,екб...)
     */
    static function getSectionsWithCardTransferPrice()
    {
        $out = array();
        $out = self::getFullSectionsID(self::KVADRIK_MAIN_SECTION_ID);
        return $out;
    }

    /*
     * get product price
     */
    static function getProductPrice($productID = 0, $dateFrom = '')
    {
        if ($productID <= 0)
            return 0;
        /*
         * search in reestr
         */
        if ($dateFrom)
            if (($oldPrice = Reestr::GetPrice(
                    array(
                        "UF_ITEM" => $productID,
                        ">=UF_DATE" => $dateFrom
                    )
                )) !== false
            )
                return $oldPrice;

        /*
         * get current
         */
        if ($res = CIBlockElement::GetList(
            array(),
            array(
                'IBLOCK_ID' => self::IBLOCK_ID,
                'ID' => $productID
            ),
            false,
            array("nTopCount" => 1),
            array("PROPERTY_PRICE")
        )->Fetch()
        )
            return $res["PROPERTY_PRICE_VALUE"];

        return 0;
    }

    /*
     * проверка - есть ли секции (ИД) среди указнного списка
     */
    static function checkSectionInProductList($list)
    {
        if (!is_array($list)) $list = array($list);
        elseif (empty($list)) return false;

        if (!CIBlockSection::GetList(
            array(),
            array(
                'IBLOCK_ID' => self::IBLOCK_ID,
                'ACTIVE' => 'Y',
                'CHECK_PERMISSIONS' => 'N',
                'ID' => $list
            ),
            false,
            array('ID'),
            array(
                'iNumPage' => 1,
                'nPageSize' => 1
            )
        )->Fetch()
        )
            return false;

        return true;
    }

    /*
     * вытащим линейный список разделов
     */
    static function getLinearSectionList($exceptSections = false, $arUfProps = false)
    {
        $sectionList = array();

        $addZeroSection = true;//флаг исключения корневой категории
        if (is_numeric($exceptSections))
            $exceptSections = array($exceptSections);
        if (is_array($exceptSections) && !empty($exceptSections)) {
            $exceptSections = array_unique(array_map('intval', $exceptSections));
            if (in_array(0, $exceptSections))
                $addZeroSection = false;
        }

        if ($addZeroSection)
            $sectionList[] = array(
                'ID' => 0,
                'NAME' => 'Без категории',
                'PARENT' => 0
            );

        $arFilter = array(
            'IBLOCK_ID' => self::IBLOCK_ID,
            'GLOBAL_ACTIVE' => 'Y',
            'CHECK_PERMISSIONS' => 'N'
        );
        $arSelect = array(
            'ID',
            'NAME',
            'IBLOCK_SECTION_ID',
            'ACTIVE'
        );

        if (!empty($arUfProps))
            $arSelect = array_merge($arSelect, $arUfProps);

        if ($exceptSections)
            $arFilter['!ID'] = $exceptSections;
        $db = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC', 'NAME' => 'ASC'),
            $arFilter,
            false,
            $arSelect
        );
        while ($res = $db->Fetch()) {
            $res['PARENT'] = intval($res['IBLOCK_SECTION_ID']);
            $sectionList[$res['ID']] = $res;
        }
        return $sectionList;
    }

    static function getSectionRootID($sectionID, $sectionList = false)
    {
        if (!is_array($sectionList))
            $sectionList = self::getLinearSectionList();

        if (!key_exists($sectionID, $sectionList)) return 0;
        $sectionInfo = $sectionList[$sectionID];
        if (!$sectionInfo['PARENT']) return $sectionID;
        return self::getSectionRootID($sectionInfo['PARENT'], $sectionList);
    }

    /*
     * возвращает название раздела + всю цепочку родителей, разделенных $delimeter
     * ParentName {$delimeter} SubParentName {$delimeter} SectionName
     */
    static function getFullSectionName($sectionInfo, $delimeter = false)
    {
        if (!$sectionInfo || empty($sectionInfo)) return '';

        if (!isset($sectionInfo['PARENT_NAMES']) || empty($sectionInfo['PARENT_NAMES']))
            return trim($sectionInfo['NAME']);

        $arSectionName = $sectionInfo['PARENT_NAMES'];
        $arSectionName[] = $sectionInfo['NAME'];
        if (!$delimeter) $delimeter = '&raquo;';
        return implode(' ' . trim($delimeter) . ' ', array_map('trim', $arSectionName));
    }

    /*
     *
     */
    static function generateLinearParents(&$sectionList)
    {
        $parentNames = $parentIDs = array();
        $prevParent = $prevParentName = false;
        $root = 0;
        foreach ($sectionList as &$sectionInfo) {
            $parent = intval(isset($sectionInfo['PARENT']) ? $sectionInfo['PARENT'] : $sectionInfo['IBLOCK_SECTION_ID']);
            if ($prevParent !== false) {
                if (!$parent) {
                    $parentNames = $parentIDs = array();
                } elseif ($prevParent == $parent) {
                    //do nothing
                } elseif (isset($parentNames[$parent])) {
                    $cut = 0;
                    foreach ($parentNames as $id => $name) {
                        if ($id == $parent) break;
                        $cut++;
                    }
                    $parentNames = array_slice($parentNames, 0, $cut, true);
                    $parentIDs = array_slice($parentIDs, 0, $cut, true);
                } elseif ($prevParent != $sectionInfo['ID']) {
                    $parentNames[$prevParent] = $prevParentName;
                    $parentIDs[] = $parent;
                }

                if (!$parent) $root = intval($sectionInfo['ID']);
                else $sectionInfo['ROOT'] = $root;
            }

            if (!empty($parentNames)) {
                $sectionInfo['PARENT_NAMES'] = $parentNames;
                $sectionInfo['PARENT_IDS'] = $parentIDs;
            }

            $prevParent = $parent;
            $prevParentName = $sectionInfo['NAME'];
        }
        unset($sectionInfo);

        return $sectionList;
    }

    static function getProductByName($name, $exceptSections = false, $limit = 150)
    {
        if (!$name = trim($name))
            return false;

        $names = array_map('trim', explode(' ', $name));
        $name = '%' . (count($names) > 1 ? implode('% && %', $names) : $name) . '%';

        $exceptZeroSection = false;//флаг исключения корневой категории
        if (is_numeric($exceptSections))
            $exceptSections = array($exceptSections);
        if (is_array($exceptSections) && !empty($exceptSections)) {
            if (in_array(0, $exceptSections) || in_array(false, $exceptSections))
                $exceptZeroSection = true;
        }

        $list = [];

        $arFilter = [
            'IBLOCK_ID' => self::IBLOCK_ID,
            'ACTIVE' => 'Y',
            '?NAME' => $name
        ];
        if ($exceptSections) {
            $arFilter['!SECTION_ID'] = $exceptSections;
            $arFilter['SECTION_GLOBAL_ACTIVE'] = 'Y';
        } elseif ($exceptZeroSection)
            $arFilter['SECTION_GLOBAL_ACTIVE'] = 'Y';


        //search in iblocks
        $db = CIBlockElement::GetList(
            ['NAME' => 'ASC'],
            $arFilter,
            false,
            ($limit = intval($limit)) > 0
                ? ['nTopCount' => $limit]
                : false,
            ['ID', 'NAME', 'IBLOCK_SECTION_ID', 'PROPERTY_PRICE', 'CODE']
        );
        while ($res = $db->Fetch()) {
            $list[] = [
                'id' => $res['ID'],
                'name' => trim($res['NAME']),
                'code' => trim($res['CODE']),
                'section' => intval($res['IBLOCK_SECTION_ID']),
                'price' => intval($res['PROPERTY_PRICE_VALUE'])
            ];

        }

        return $list;
    }

    static function getSectionByName($name, $exceptSections = false, $limit = 150)
    {
        if (!$name = trim($name))
            return false;

        $names = array_map('trim', explode(' ', $name));
        $name = '%' . (count($names) > 1 ? implode('% && %', $names) : $name) . '%';

        $exceptZeroSection = false;//флаг исключения корневой категории
        if (is_numeric($exceptSections))
            $exceptSections = array($exceptSections);
        if (is_array($exceptSections) && !empty($exceptSections)) {
            if (in_array(0, $exceptSections) || in_array(false, $exceptSections))
                $exceptZeroSection = true;
        }

        $list = array();

        $arFilter = array(
            'IBLOCK_ID' => self::IBLOCK_ID,
            'ACTIVE' => 'Y',
            '?NAME' => $name
        );
        if ($exceptSections) {
            $arFilter['!ID'] = $exceptSections;
            $arFilter['GLOBAL_ACTIVE '] = 'Y';
        } elseif ($exceptZeroSection)
            $arFilter['GLOBAL_ACTIVE '] = 'Y';


        //search in iblocks
        $db = CIBlockSection::GetList(
            array('LEFT_MARGIN' => 'ASC', 'NAME' => 'ASC'),
            $arFilter,
            false,
            array('ID', 'NAME', 'IBLOCK_SECTION_ID'),
            ($limit = intval($limit)) > 0
                ? array('nTopCount' => $limit)
                : false
        );
        while ($res = $db->Fetch()) {
            $list[] = array(
                'id' => $res['ID'],
                'name' => trim($res['NAME']),
                'parent' => intval($res['IBLOCK_SECTION_ID']),
            );

        }

        return $list;
    }

    static function getProductName($id)
    {
        return self::getProduct($id, 'NAME');
    }

    static function getProduct($id, $fields = false)
    {
        if (($id = intval($id)) <= 0)
            return false;

        if (!$fields)
            $fields = array(
                'ID',
                'IBLOCK_ID',
                'NAME',
                'ACTIVE'
            );
        elseif (!is_array($fields))
            $fields = array($fields);

        if (!$product = CIBlockElement::GetList(
            array(),
            array(
                'IBLOCK_ID' => self::IBLOCK_ID,
                'ACTIVE' => 'Y',
                'ID' => $id
            ),
            false,
            array('nTopCount' => 1),
            $fields
        )->Fetch()
        )
            return false;
        return $product;
    }

    static function getProductRootSection($id)
    {
        $productID = intval($id);
        if ($productID <= 0) return false;

        return ($resSection = CIBlockSection::GetList(
            array(),
            array(
                'IBLOCK_ID' => self::IBLOCK_ID,
                'SECTION_ID' => false,
                'HAS_ELEMENT' => $productID
            ),
            false,
            array('ID')
        )->Fetch()) ? $resSection['ID'] : false;
    }


    /*
     * обновить цены иностранной валюты
     */
    static function updateForeignPrice($pricePrefix)
    {
        $mt = microtime(true);

        $pricePrefix = strtoupper(trim($pricePrefix));

        //get price prefix
        $priceAlpha = 0;
        if ($pricePrefix === 'KZ') $priceAlpha = 5.9;
        elseif ($pricePrefix === 'BLR') $priceAlpha = 1 / 33;
        else return false;

        $db = CIBlockElement::GetList(
            array(),
            array(
                'IBLOCK_ID' => Product::IBLOCK_ID,
                '!SECTION_ID' => array(
                    self::SECTION_SERVICE,
                    self::SECTION_NEW
                ),
                '!PROPERTY_PRICE' => false,
                '>PROPERTY_PRICE' => 0,
            ),
            false,
            false,
            array('ID', 'PROPERTY_PRICE', 'PROPERTY_PRICE_CARD', 'PROPERTY_PRICE_' . $pricePrefix, 'NAME', 'IBLOCK_SECTION_ID')
        );
        while ($res = $db->Fetch()) {
            if (microtime(true) - $mt > 55) return false;
            if (!$priceCard = intval($res['PROPERTY_PRICE_CARD_VALUE']))
                $priceCard = intval($res['PROPERTY_PRICE_VALUE']);

            $priceForeign = intval($res['PROPERTY_PRICE_' . $pricePrefix . '_VALUE']);
            $newPriceForeign = ceil($priceCard * $priceAlpha);

            if ($newPriceForeign == $priceForeign) continue;

            CIBlockElement::SetPropertyValuesEx($res['ID'], Product::IBLOCK_ID, array('PRICE_' . $pricePrefix => $newPriceForeign));
        }

        return true;
    }
}