<?
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;

if($_SERVER['SERVER_ADDR'] != $_SERVER['REMOTE_ADDR']) {
    die();
} else {
    require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';


    $request = Application::getInstance()->getContext()->getRequest();


    /**
     * Настраиваем фильтрацию
     */
    $city = $request->getPost('city');
    $dirs = $request->getPost('dirs');
    $type = $request->getPost('type');

    $city_f = false;
    if ($city) {
        $city_f = array(
            "LOGIC" => "OR"
        );
        foreach($city as $c) {
            array_push($city_f, array("UF_CITY" => $c));
        }
    }

    $dirs_f = false;
    if ($dirs) {
        $dirs_f = array(
            "LOGIC" => "OR"
        );
        foreach($dirs as $d) {
            array_push($dirs_f, array("UF_DIRECTION" => $d));
        }
    }

    $type_f = false;
    if ($type) {
        $type_f = array(
            "LOGIC" => "OR"
        );
        foreach($type as $t) {
            array_push($type_f, array("UF_ADV" => $t));
        }
    }

    $f = array(
        "LOGIC" => "AND",
        "!UF_VALUE" => false
    );
    if ($city_f !== false) {
        array_push($f, $city_f);
    }
    if ($dirs_f !== false) {
        array_push($f, $dirs_f);
    }
    if ($type_f !== false) {
        array_push($f, $type_f);
    }

    /**
     * Для отфутболивания по дате
     */
    $tmp = explode(' - ', $request['date']);
    $reqFrom = new \DateTime($tmp[0]);
    $reqTo = new \DateTime($tmp[1]);
    $from_ts = $reqFrom->getTimestamp();
    $to_ts = $reqTo->getTimestamp();

    Loader::includeModule("highloadblock");
    Loader::includeModule("iblock");
    Loader::includeModule("isection");
    $entity = HL\HighloadBlockTable::compileEntity(45);
    $entityDataClass = $entity->getDataClass();
    $result = $entityDataClass::getList(array(
        "select" => array("UF_VALUE", "UF_CITY", "UF_DIRECTION", "UF_DATE"),
        "order" => array("ID" => "DESC"),
        "filter" => $f,
    ));

    $res = [];
    $total_cities = [];
    $total_cities_names = [];
    while ($arRow = $result->Fetch()) {
        $tmp = $arRow['UF_DATE']->getTimestamp();

        /**
         * Если входит в диапазон дат
         */
        if ($tmp >= $from_ts && $tmp <= $to_ts) {
            if (!isset($res[$arRow['UF_DIRECTION']])) {
                $direction_name = CIBlockSection::GetByID($arRow['UF_DIRECTION']);
                if ($direction_name_res = $direction_name->GetNext()) {
                    $dn = $direction_name_res['NAME'];
                } else {
                    $dn = '';
                }
                $res[$arRow['UF_DIRECTION']] = [
                    'name' => $dn,
                    'cities' => []
                ];
            }
            $t = $res[$arRow['UF_DIRECTION']];
            $t2 = $t['cities'];
            if (!isset($t2[$arRow['UF_CITY']])) {
                $city_name = CIBlockElement::GetByID($arRow['UF_CITY']);
                if ($city_name_res = $city_name->GetNext()) {
                    $cn = $city_name_res['NAME'];
                } else {
                    $cn = '';
                }
                if (!in_array($arRow['UF_CITY'], $total_cities)) {
                    array_push($total_cities, $arRow['UF_CITY']);
                    $total_cities_names[$arRow['UF_CITY']] = $cn;
                }

                $t2[$arRow['UF_CITY']] = [
                    'id' => $arRow['UF_CITY'],
                    'name' => $cn,
                    'value' => $arRow['UF_VALUE']
                ];
            } else {
                $t2[$arRow['UF_CITY']]['value'] += $arRow['UF_VALUE'];
            }
            //$t2[$arRow['UF_CITY']]['value'] = round($arRow['UF_VALUE'], 2);
            $t['cities'] = $t2;
            $res[$arRow['UF_DIRECTION']] = $t;
        }
    }

    /**
     * Добавляем пустые поля на фронт
     */
    $refactor = [];
    foreach ($res as $r) {
        $cheked = [];
        $tval = 0;
        foreach ($r['cities'] as $rd) {
            if (in_array($rd['id'], $total_cities)) {
                array_push($cheked, $rd['id']);
            }
            $tval += $rd['value'];
        }
        $r['total'] = $tval;
        $raznica = array_diff($total_cities, $cheked);
        foreach ($raznica as $raz) {
            $r['cities'][$raz] = [
                'id' => $raz,
                'name' => $total_cities_names[$raz],
                'value' => ''
            ];
        }
        array_push($refactor, $r);
    }

    echo json_encode($refactor);
    die();
}