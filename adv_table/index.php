<?
use Bitrix\Main\Page\Asset;
if (isset($_REQUEST['ed_ajax'])) {
    require_once($_SERVER["DOCUMENT_ROOT"] . "/adv_table/ajax.php");
} else {

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
    $APPLICATION->SetTitle("Отчёт по расходам на рекламу");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/ed/daterangepicker.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/ed/select2.css");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/ed/moment.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/ed/daterangepicker.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/ed/select2.min.js");


    Asset::getInstance()->addCss("https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css");
    Asset::getInstance()->addCss("//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css");
    Asset::getInstance()->addJs("//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js");

    ?>

    <div class="ed-wrapper">
        <h1>Отчёт по расходам на рекламу</h1>
        <div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Даты</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="input-group date">
                        <input data-ed="rateDateFromTo" required type="text" class="form-control" name="rateDateFromTo">
                        <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Направления</label>
                <div class="col-lg-6">
                    <select required class="form-control kt-select2 select2-hidden-accessible" id="edRatedirsInput" name="ratedirs[]" multiple="multiple">
                        <option></option>
                        <?
                        $IBLOCK_ID = 17;
                        $arFilter = array(
                            'IBLOCK_ID' => $IBLOCK_ID,
                            'GLOBAL_ACTIVE' => 'Y',
                            'CHECK_PERMISSIONS' => 'N',
                            'SECTION_ID' => false
                        );
                        $db = CIBlockSection::GetList(
                            array('LEFT_MARGIN' => 'ASC', 'NAME' => 'ASC'),
                            $arFilter,
                            true,
                            array(
                                'ID',
                                'NAME',
                                'IBLOCK_SECTION_ID',
                                'GLOBAL_ACTIVE'
                            )
                        );
                        $groups = [];
                        $t = 0;
                        while ($res = $db->Fetch()) {
                            $total = 0;
                            $options2 = '';
                            $arFilter2 = array(
                                'IBLOCK_ID' => $IBLOCK_ID,
                                'GLOBAL_ACTIVE' => 'Y',
                                'CHECK_PERMISSIONS' => 'N',
                                'SECTION_ID' => $res['ID']
                            );
                            $db2 = CIBlockSection::GetList(
                                array('LEFT_MARGIN' => 'ASC', 'NAME' => 'ASC'),
                                $arFilter2,
                                true,
                                array(
                                    'ID',
                                    'NAME',
                                    'IBLOCK_SECTION_ID',
                                    'GLOBAL_ACTIVE'
                                )
                            );
                            while ($res2 = $db2->Fetch()) {
                                $total++;
                                $options2 .= '<option value="'.$res2['ID'].'">'.$res2['NAME'].' ('.$res2['ELEMENT_CNT'].')</option>';
                            }
                            if ($total == 0) {
                                echo '<option value="'.$res['ID'].'">'.$res['NAME'].' ('.$res['ELEMENT_CNT'].')</option>';
                            } else {
                                echo '<optgroup label="'.$res['NAME'].' ('.$res['ELEMENT_CNT'].')">'.$options2.'</optgroup>';
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Города</label>
                <div class="col-lg-6">
                    <select required class="form-control kt-select2 select2-hidden-accessible" id="edRatecityInput" name="ratecity[]" multiple="multiple">
                        <option></option>
                        <?
                        $IBLOCK_ID = 16;
                        $arFilter = array(
                            'IBLOCK_ID' => $IBLOCK_ID,
                            'GLOBAL_ACTIVE' => 'Y',
                            'CHECK_PERMISSIONS' => 'N'
                        );
                        $db = CIBlockElement::GetList(
                            array('LEFT_MARGIN' => 'ASC', 'NAME' => 'ASC'),
                            $arFilter,
                            false,
                            false,
                            array(
                                'ID',
                                'NAME',
                                'IBLOCK_SECTION_ID',
                                'GLOBAL_ACTIVE'
                            )
                        );
                        $groups = [];
                        $t = 0;
                        while ($res = $db->Fetch()) {
                            echo '<option value="'.$res['ID'].'">'.$res['NAME'].'</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Тип</label>
                <div class="col-lg-6">
                    <select class="form-control kt-select2 select2-hidden-accessible" id="edRatetypeInput" name="ratetype[]" multiple="multiple">
                        <option></option>
                        <?
                        $IBLOCK_ID = 49;
                        $arFilter = array(
                            'IBLOCK_ID' => $IBLOCK_ID,
                            'GLOBAL_ACTIVE' => 'Y',
                            'CHECK_PERMISSIONS' => 'N'
                        );
                        $db = CIBlockElement::GetList(
                            array('LEFT_MARGIN' => 'ASC', 'NAME' => 'ASC'),
                            $arFilter,
                            false,
                            false,
                            array(
                                'ID',
                                'NAME',
                                'IBLOCK_SECTION_ID',
                                'GLOBAL_ACTIVE'
                            )
                        );
                        $groups = [];
                        $t = 0;
                        while ($res = $db->Fetch()) {
                            echo '<option value="'.$res['ID'].'">'.$res['NAME'].'</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <button class="btn btn-success" data-ed="submit-data">Отобразить данные</button>
            </div>
        </div>
        <div class="data-res-wrapper" data-ed="data-res-wrapper">
            <div class="data-res-loader" data-ed="data-res-loader">

            </div>
            <div class="data-res" data-ed="data-res">

            </div>
            <div class="data-res-error" data-ed="data-res-error">

            </div>
        </div>

    </div>
    <style>
        .ed-wrapper {
            padding-bottom: 90px;
            width: 90%;
            margin: 0% 5%;
        }
        .data-res-error {
            background-color: #ff7d7d;
            align-items: center;
            justify-content: center;
            padding: 15px;
            display:none;
        }
        .data-res-error p {
            margin:0px;
            font-weight:600;
        }
    </style>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            $('[data-ed="submit-data"]').on('click', function(e) {
                e.preventDefault();
                $('.data-res-error').css({
                    display: 'none'
                });
                $.ajax({
                    url: '.',
                    data: {
                        'ed_ajax': 1,
                        'date': $('[data-ed="rateDateFromTo"]').val(),
                        'dirs': $('#edRatedirsInput').val(),
                        'city': $('#edRatecityInput').val(),
                        'type': $('#edRatetypeInput').val()
                    },
                    type: "POST",
                    success: function (response) {
                        console.log(response);
                        data = JSON.parse(response);
                        console.log(data);
                        if (data.length === 0) {
                            $('[data-ed="data-res-error"]').html('<p>Расходов на рекламу с указанными параметрами не найдено.</p>')
                            $('.data-res-error').css({
                                display: 'flex'
                            });
                            $('[data-ed="data-res"]').html('');
                            if (typeof _LOADER !== undefined && _LOADER.is(':visible')) {
                                _LOADER.hide('drop', {direction: getRandomArrayElement()}, 300);
                            }
                            return;
                        } else {
                            thead = data[0].cities;

                            table = '<table id="data-res" class="display" style="width:100%">';
                            table += '<thead>';
                            table += '<tr><th></th>';
                            for(var i in thead) {
                                table += '<th>'+thead[i].name+'</th>';
                            }
                            table += '<th>Всего</th>';
                            table += '</tr>';
                            table += '</thead>';


                            table += '<tbody>';
                            for(var j in data) {
                                table += '<tr>';
                                table += '<td>'+data[j].name+'</td>';
                                for(var i in data[j].cities) {
                                    table += '<td>'+data[j].cities[i].value+'</td>';
                                }
                                table += '<td>'+data[j].total+'</td>';
                                table += '</tr>';
                            }

                            table += '</tbody>';
                            table += '</table>';
                            $('[data-ed="data-res"]').html(table);

                            $('#data-res').DataTable({
                                responsive: true,
                                "scrollX": true,
                                language: {
                                    url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
                                }
                            });
                        }

                        //console.log(JSON.parse(response));
                    }
                });
            });
            window.dataRangePickerEdLocale = {
                "format": "DD.MM.YYYY",
                "separator": " - ",
                "applyLabel": "Сохранить",
                "cancelLabel": "Отмена",
                "fromLabel": "С",
                "toLabel": "По",
                "customRangeLabel": "Custom",
                "daysOfWeek": [
                    "Вс",
                    "Пн",
                    "Вт",
                    "Ср",
                    "Чт",
                    "Пн",
                    "Сб"
                ],
                "monthNames": [
                    "Январь",
                    "Февраль",
                    "Март",
                    "Апрель",
                    "Май",
                    "Июнь",
                    "Июль",
                    "Август",
                    "Сентябрь",
                    "Октябрь",
                    "Ноябрь",
                    "Декабрь"
                ],
                "firstDay": 1
            };
            $('[data-ed="rateDateFromTo"]').daterangepicker({
                opens: 'right',
                locale: window.dataRangePickerEdLocale
            });
            $('#edRatedirsInput').select2({
                language: "ru",
                placeholder: 'Выберите направления',
                width: '100%'
            });
            $('#edRatecityInput').select2({
                language: "ru",
                placeholder: 'Выберите города',
                width: '100%'
            });
            $('#edRatetypeInput').select2({
                language: "ru",
                placeholder: 'Выберите тип рекламы',
                width: '100%'
            });
        });
    </script>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
}